# Тестовый проект на базе фреймворка FuelPHP #

**Реализовано ряд фич**
*  Статические разделы [About](https://gitlab.com/wdforge/fuelnews/-/blob/master/fuel/app/classes/controller/pages.php#L12) и [Contacts](https://gitlab.com/wdforge/fuelnews/-/blob/master/fuel/app/classes/controller/pages.php#L21)
*  Контроллер вывода списка новостей [Controller_News](https://gitlab.com/wdforge/fuelnews/-/blob/master/fuel/app/classes/controller/news.php)
*  Модель новости [Model_News](https://gitlab.com/wdforge/fuelnews/-/blob/master/fuel/app/classes/model/news.php)
*  Обсервер модели новости [Observer_News](https://gitlab.com/wdforge/fuelnews/-/blob/master/fuel/app/classes/observer/news.php)
*  Презентер модели новости [Presenter_News](https://gitlab.com/wdforge/fuelnews/-/blob/master/fuel/app/classes/presenter/news.php)
*  Модуль с FullREST контроллером [Newscrud::Controller_Api](https://gitlab.com/wdforge/fuelnews/-/blob/master/fuel/app/modules/newscrud/classes/controller/api.php)
*  Тестовая задача [Newscrud::Testjob](https://gitlab.com/wdforge/fuelnews/-/blob/master/fuel/app/modules/newscrud/tasks/testjob.php)

**Запуск тестовой задачи**

`php oil refine newscrud::testjob:load`

## CRUD операции ##

**Получение записи**
*  GET /newscrud/api/news/<:id>

**Обновление записи**
*  POST /newscrud/api/news/<:id>

Параметры:
* title
* body

**Новая запись**
*  PUT /newscrud/api/news

Параметры:
* title
* body

**Удаление записи**
*  DELETE /newscrud/api/news/<:id>
