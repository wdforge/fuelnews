<?php

class Model_News extends \Orm\Model
{
    protected static $_properties = array(
        "id" => array(
            "label" => "Id",
            "data_type" => "int",
        ),
        "title" => array(
            "label" => "Title",
            "data_type" => "varchar",
        ),
        "body" => array(
            "label" => "Body",
            "data_type" => "text",
        ),
        "created_at" => array(
            "label" => "Created at",
            "data_type" => "int",
        ),
        "updated_at" => array(
            "label" => "Updated at",
            "data_type" => "int",
        ),
    );

    protected static $_observers = array(
        'News' => array(
            'events' => array('before_update', 'after_update'),
        ),
    );

    protected static $_table_name = 'news';
    protected static $_primary_key = array('id');

    /**
     * Постраничная выборка списка
     *
     * @return array
     */
    public static function paginate()
    {
        return Model_News::query()
            ->rows_offset(\Config::get('pagination.offset', 0))
            ->rows_limit(\Config::get('pagination.per_page', 3))
            ->get();
    }
}
