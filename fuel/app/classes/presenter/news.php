<?php

class Presenter_News extends Presenter
{
    public function view()
    {
        $news_list = Model_News::paginate();
        $news = [];

        foreach ($news_list as $news_one) {
            $news[] = Html::anchor('#', $news_one->title, [
                'class' => 'news-href',
                'data-href' => '/news/view/' . $news_one->id
            ]);
        }

        $this->set_safe('news', $news);
    }
}