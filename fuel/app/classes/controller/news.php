<?php

/**
 * Class Controller_Newslist
 */
class Controller_News extends Controller_Generic
{
    /**
     * Вывод списка с пагинацией
     */
    public function action_index()
    {
        $this->template->title = 'Список новостей';
        $this->template->content = Presenter::forge('news');

        $pagination = Pagination::forge('pagination', [
            'pagination_url' => \Config::get('pagination.pagination_url'),
            'total_items' => Model_News::count(),
            'per_page' => \Config::get('pagination.per_page', 10),
            'uri_segment' => \Config::get('pagination.uri_segment', 2)
        ]);

        \Config::set('pagination.paginator', $pagination);
        \Config::set('pagination.offset', $pagination->offset);
    }

    /**
     * Добавление новости
     */
    public function action_create()
    {
        $this->template->title = 'Список новостей';
        $this->template->content = Presenter::forge('news');
    }

    /**
     * Вывод одной новости
     *
     * @param $id
     * @return Response
     */
    public function action_view($id)
    {
        $content_type = ['Content-type'=>'application/json'];

        /**
         * @var $request Request
         */
        $request = Request::forge('newscrud/api/news/'.$id)
            ->set_method('GET')
            ->execute();

        $response = $request->response();
        return new \Response($response->body(), $response->status, $content_type);
    }
}