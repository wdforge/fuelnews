<?php

/**
 * Class Controller_Pages
 */
class Controller_Pages extends Controller_Generic
{

    /**
     * Страница об аладельце
     */
    public function action_about()
    {
        $this->template->title = 'Pages &raquo; About';
        $this->template->content = View::forge('pages/about');
    }

    /**
     * Страница с контактами
     */
    public function action_contacts()
    {
        $this->template->title = 'Pages &raquo; Contacts';
        $this->template->content = View::forge('pages/contacts');
    }

}
