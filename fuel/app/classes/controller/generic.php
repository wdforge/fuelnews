<?php


class Controller_Generic extends Controller_Template
{
    /**
     * Load the template and create the $this->template object
     */
    public function before()
    {
        parent::before();
        $this->template->menu = \View::forge('./partials/menu.php');
    }

}