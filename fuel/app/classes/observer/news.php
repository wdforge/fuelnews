<?php

/**
 * Class Model_Observer_NewsBeforeUpdate
 */
class Observer_News extends Orm\Observer
{
    /**
     * Действие после обновления
     *
     * @param \Orm\Model $model
     */
    public function before_update(Orm\Model $model)
    {
        \Log::info('Succesfully saved object of class '.get_class($model));
    }

    /**
     * Действие после обновления
     *
     * @param \Orm\Model $model
     */
    public function after_update(Orm\Model $model)
    {
        $model->title = "Observer change: ".date('Y-m-d H:i:s');
        $model->save();
    }

}