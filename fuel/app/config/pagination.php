<?php
return [
    'pagination_url' => '/news/',
    'per_page' => 3,
    'uri_segment' => 2
];
