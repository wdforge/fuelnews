<div class="row">
    <div class="col-lg-6">
        <table class="table-bordered">
            <?php foreach ($news as $news_one): ?>
                <tr>
                    <td>
                        <?=$news_one?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <div class="col-lg-6">
        <div class="h1 panel-title" id="news-title"></div>
        <div class="text-info" id="news-body"></div>
    </div>
</div>
<?= \Config::get('pagination.paginator') ?>
