<?php

namespace Fuel\Tasks;

class Testjob
{

    /**
     * This method gets ran when a valid method name is not used in the command.
     *
     * Usage (from command line):
     *
     * php oil r mymodule::newsparser
     *
     * @return string
     */
    public function run($args = NULL)
    {
        echo "\n===========================================";
        echo "\nRunning DEFAULT task [Newscrud::Testjob:Run]";
        echo "\n-------------------------------------------\n\n";

        /***************************
         * Put in TASK DETAILS HERE
         **************************/
        \Cli::color("\nRUN PROGRESS!!!\n", 'red');
    }


    /**
     * This method gets ran when a valid method name is not used in the command.
     *
     * Usage (from command line):
     *
     * php oil r mymodule::newsparser:load "arguments"
     *
     * @return string
     */
    public function load($args = NULL)
    {
        echo "\n===========================================";
        echo "\nRunning task [Newscrud::Testjob:Load]";
        echo "\n-------------------------------------------\n\n";

        /***************************
         * Put in TASK DETAILS HERE
         **************************/
        echo \Cli::color("\nLOAD PROGRESS!!!\n", 'blue');
    }

}
/* End of file mymodule/tasks/newsparser.php */
