<?php

return [
    'ERROR_SAVE_MSG' => 'Could not save news',
    'ERROR_SETID_MSG' => 'Not able to set id for record',
    'ERROR_LOAD_NEWS' => 'Could not load news'
];