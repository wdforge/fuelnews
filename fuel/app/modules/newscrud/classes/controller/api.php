<?php

namespace Newscrud;


class Controller_Api extends \Controller_Rest
{

    /**
     * Получение новости по id
     *
     * @return \Orm\Model|\Orm\Model[]|\Orm\Query|null
     */
    public function get_news($id)
    {
        if (!$success = boolval($id)) {
            return [
                'success' => false,
                'error' => \Config::get('errors.ERROR_SETID_MSG'),
                'data' => false
            ];
        }

        if (!$news = \Model_News::find($id)) {
            return [
                'success' => false,
                'error' => \Config::get('errors.ERROR_LOAD_NEWS'),
                'data' => false
            ];
        }

        return [
            'success' => true,
            'error' => '',
            'data' => $news->to_array()
        ];
    }

    /**
     * Запись изменений новости
     *
     * @return array
     * @throws \Exception
     */
    public function patch_news($id)
    {
        if (!$success = boolval($id)) {
            return [
                'success' => false,
                'error' => \Config::get('errors.ERROR_SETID_MSG'),
                'data' => false
            ];
        }

        if (!$news = \Model_News::find($id)) {
            return [
                'success' => false,
                'error' => \Config::get('errors.ERROR_LOAD_NEWS'),
                'data' => false
            ];
        }

        $news->title = \Input::param('title');
        $news->body = \Input::param('body');

        if (!$news->save()) {
            return [
                'success' => false,
                'error' => \Config::get('errors.ERROR_SAVE_MSG')
            ];
        }

        return [
            'success' => true,
            'error' => '',
            'data' => false
        ];
    }

    /**
     * Добавление новости
     *
     * @return array
     * @throws \Exception
     */
    public function post_news()
    {
        $error = '';
        $success = true;

        $news = \Model_News::forge([
            'title' => \Input::param('title'),
            'body' => \Input::param('body'),
        ]);

        if (!$news->save()) {
            $success = false;
            $error = static::ERROR_SAVE_MSG;
        }

        return [
            'success' => $success,
            'error' => $error
        ];
    }

    /**
     * Удаление новости
     *
     * @return bool[]|false[]
     * @throws \Exception
     */
    public function delete_news($id)
    {
        if (!$success = boolval($id)) {
            return [
                'success' => false,
                'error' => \Config::get('errors.ERROR_SETID_MSG'),
                'data' => false
            ];
        }

        if (!$news = \Model_News::find($id)) {
            return [
                'success' => false,
                'error' => \Config::get('errors.ERROR_LOAD_NEWS')
            ];
        }

        $success = (boolean)$news->delete();

        return [
            'success' => $success,
            'error' => ''
        ];
    }
}
