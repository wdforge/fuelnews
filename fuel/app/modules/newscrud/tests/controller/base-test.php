<?php

namespace Newscrud;


abstract class Tests_Controller_BaseTest extends \TestCase
{
    protected static $_table;
    protected static $_fields = [];
    protected static $_rows = [];
    protected static $_pk = ['id'];
    protected static $_model_class;

    /**
     * Настройка ресурсов
     */
    protected function setUp(): void
    {
    }

    /**
     * Заполнение данными БД
     *
     * @param array $records
     * @throws \Database_Exception
     */
    protected function fill_data($records = [])
    {
        if (\DBUtil::table_exists(static::$_table)) {
            \DBUtil::drop_table(static::$_table);
        }

        \DBUtil::create_table(static::$_table, static::$_fields, static::$_pk);
        $class = static::$_model_class;

        foreach ($records as $record) {
            $model = $class::forge($record);
            $model->save();
        }
    }

    /**
     * Удаление таблицы
     */
    protected function drop_table()
    {
        \DBUtil::drop_table(static::$_table);
    }

}