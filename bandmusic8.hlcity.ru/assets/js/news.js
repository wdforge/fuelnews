jQuery(document).ready(function () {
    jQuery('.news-href').click(function () {
        let href = jQuery(this).data('href');
        if (href) {
            jQuery.get(href, function (data) {
                jQuery('#news-title').html(data.data.title);
                jQuery('#news-body').html(data.data.body);
            });
        }

        return false;
    })
});